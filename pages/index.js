import React from 'react';
import sounds from '../components/Sounds.js'
import { ThemeProvider, createTheme, SoundsProvider, withSounds, createSounds, Button, Arwes  } from 'arwes';

const createdTheme = createTheme()
const createdSounds = sounds

// const App = (props) => (
//   <ThemeProvider theme={createdTheme}>
//    <Arwes>
//       <p>A SciFi Project</p>
//       <SoundsProvider sounds={createdSounds}>
//         <Button animate>Click me</Button>
//       </SoundsProvider>
//     </Arwes>
//   </ThemeProvider>
// );

var alreadyRandered = false;

class App extends React.Component {

    render() {
        if (!alreadyRandered) {
            alreadyRandered = true;

            return (
                <ThemeProvider theme={createdTheme}>
                    <Arwes>
                        <p>A SciFi Project</p>
                        <SoundsProvider sounds={createdSounds}>
                            <Button animate>Click me</Button>
                        </SoundsProvider>
                    </Arwes>
                </ThemeProvider>
            )
        } 

        return(<div/>)
    }
}

export default App