## Configuration

### System Variables
PORT: port to start server on
NODE_ENV: 'production' -> don't start in development mode

## Local Build Setup
npm install --save react react-dom next express arwes isomorphic-unfetch

