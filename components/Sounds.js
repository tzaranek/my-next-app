import { ThemeProvider, createTheme, SoundsProvider, withSounds, createSounds, Button, Arwes  } from 'arwes';

const mySounds = {
    shared: { volume: 1, },  // Shared sound settings
    players: {  // The player settings
      click: {  // With the name the player is created
        sound: { src: ['/static/sounds/click.mp3'] }  // The settings to pass to Howler
      },
      typing: {
        sound: { src: ['/static/sounds/typing.mp3'] },
        settings: { oneAtATime: true }  // The custom app settings
      },
      deploy: {
        sound: { src: ['/static/sounds/deploy.mp3'] },
        settings: { oneAtATime: true }
      },
    }
  };

export default createSounds(mySounds)