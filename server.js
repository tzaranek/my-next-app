const next = require('next');
const express = require('express');
const routes = require('next-routes')();

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 8080;
const app = next({ dev });
const handler = routes.getRequestHandler(app);

app.prepare().then(() => {
  const server = express()

  server.use(handler)
  server.listen(port, (err) => {
      if (err) throw err
      console.log('> Listening on http://localhost:'+port)
  })

})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})
